package maestro.requestor;

import android.os.Handler;
import android.os.Message;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;

/**
 * Created by Artyom on 10/8/2014.
 */
public class Requestor {

    private static final SerialExecutor mExecutor = new SerialExecutor(Executors.newCachedThreadPool());
    private static final HashMap<String, Object> mRequestCache = new HashMap<String, Object>();
    private static ClientGetter mClientGetter = new ClientGetter();

    public static final void setClientGetter(ClientGetter client) {
        if (client != null)
            mClientGetter = client;
    }

    public enum REQUEST_ERROR {
        CONNECTION, SERVER, UNKNOWN
    }

    public interface ResponseListener {
        public void onResponse(Request request, Object object);

        public void onError(Request request, REQUEST_ERROR error);
    }

    public interface RequestPostProcessor {

        public Object process(String response);

    }

    public static class Request implements Runnable {

        public enum REQUEST_TYPE {
            GET, POST, PUT
        }

        private final int MSG_ON_RESULT = 0;
        private final int MSG_ON_ERROR = 1;

        public final Handler uiHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (Listener == null) return;
                switch (msg.what) {
                    case MSG_ON_ERROR:
                        synchronized (Listener) {
                            Listener.onError(Request.this, (REQUEST_ERROR) msg.obj);
                        }
                        break;
                    case MSG_ON_RESULT:
                        synchronized (Listener) {
                            Listener.onResponse(Request.this, msg.obj);
                        }
                        break;
                }
            }
        };

        public String Url;
        public String Format;
        public ResponseListener Listener;
        public RequestPostProcessor PostProcessor;
        public REQUEST_TYPE Type = REQUEST_TYPE.GET;
        public Object Result;
        public StringEntity Entity;
        public String[] RedirectedUrl = new String[0];
        private HttpClient mClient;
        private ClientGetter mClientGetter;
        public boolean IsRequiredStream;
        public boolean IsFromCache;
        public boolean CacheEnable = true;
        private boolean isCanceled;

        public Request(String url) {
            Url = url;
        }

        public Request setClientGetter(ClientGetter clientGetter) {
            mClientGetter = clientGetter;
            return this;
        }

        public Request setListener(ResponseListener listener) {
            Listener = listener;
            return this;
        }

        public Request setPostProcessor(RequestPostProcessor postProcessor) {
            PostProcessor = postProcessor;
            return this;
        }

        public Request setType(REQUEST_TYPE type) {
            Type = type;
            return this;
        }

        public Request setFormat(String format) {
            Format = format;
            return this;
        }

        public Request setIsRequiredStream(boolean value) {
            IsRequiredStream = value;
            return this;
        }

        public Request setIsFromCache(boolean value) {
            IsFromCache = value;
            return this;
        }

        public Request setStringEntity(StringEntity entity) {
            Entity = entity;
            return this;
        }

        public void addRedirectedUrl(String url) {
            final int size = RedirectedUrl.length;
            RedirectedUrl = Arrays.copyOf(RedirectedUrl, size + 1);
            RedirectedUrl[size] = url;
        }

        public String popRedirectUrl() {
            final int size = RedirectedUrl.length;
            return size > 0 ? RedirectedUrl[size - 1] : null;
        }

        @Override
        public void run() {
            if (mClientGetter == null) {
                mClientGetter = Requestor.mClientGetter;
            }
            mClient = mClientGetter.getClient();
            Result = sendRequest(this, mClient);
            if (Result instanceof REQUEST_ERROR) {
                uiHandler.obtainMessage(MSG_ON_ERROR, Result).sendToTarget();
            } else {
                if (!IsRequiredStream && !IsFromCache && CacheEnable) {
                    mRequestCache.put(Url, Result);
                }
                uiHandler.obtainMessage(MSG_ON_RESULT, PostProcessor == null
                        ? Result : PostProcessor.process((String) Result)).sendToTarget();
            }
        }

        public String getKey() {
            return Url;
        }

        public void async() {
            mExecutor.executeAsync(this);
        }

        public void block() {
            mExecutor.execute(this);
        }

        public Request getFromCache() {
            IsFromCache = true;
            Result = mRequestCache.get(Url);
            return this;
        }

        public void cancel() {
            isCanceled = true;
            if (mClient != null) {
                try {
                    mClient.getConnectionManager().shutdown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static Object sendRequest(Request request, HttpClient client) {
        if (!request.IsRequiredStream && mRequestCache.containsKey(request.Url)) {
            return request.getFromCache().Result;
        }

        try {
            String url = request.popRedirectUrl();
            if (url == null) url = request.Url;
            HttpRequestBase requestBase = mClientGetter.getBaseRequest(request, url);
            if (request.Entity != null) {
                if (requestBase instanceof HttpPost)
                    ((HttpPost) requestBase).setEntity(request.Entity);
                else if (requestBase instanceof HttpPut)
                    ((HttpPut) requestBase).setEntity(request.Entity);
            }
            HttpResponse response = client.execute(requestBase);
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                Header[] headers = response.getHeaders("Location");
                if (headers != null && headers.length != 0) {
                    request.addRedirectedUrl(headers[headers.length - 1].getValue());
                    return sendRequest(request, client);
                } else {
                    return null;
                }
            }

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream stream = entity.getContent();
                Header contentEncoding = response.getFirstHeader("Content-Encoding");
                if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                    stream = new GZIPInputStream(stream);
                }
                if (request.IsRequiredStream) {
                    return stream;
                } else {
                    String resultString = convertStreamToString(stream);
                    stream.close();
                    return resultString;
                }
            }
        } catch (UnknownHostException e) {
            return REQUEST_ERROR.CONNECTION;
        } catch (IOException e) {
            return REQUEST_ERROR.UNKNOWN;
        } finally {
            client.getConnectionManager().shutdown();
        }
        return REQUEST_ERROR.UNKNOWN;
    }

    public static String convertStreamToString(InputStream is) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    private static final class SerialExecutor implements Executor {
        final Queue tasks = new ArrayDeque();
        final Executor executor;
        Object active;

        SerialExecutor(Executor executor) {
            this.executor = executor;
        }

        private boolean ensure(Runnable r) {
            synchronized (tasks) {
                Request request = (Request) r;
                while (tasks.iterator().hasNext()) {
                    Request element = (Request) tasks.element();
                    if (request.getKey().equals(element.getKey())) {
                        element.Listener = request.Listener;
                        return false;
                    }
                }
            }
            return true;
        }

        public synchronized void executeAsync(final Runnable r) {
            if (ensure(r)) {
                tasks.offer(new Thread() {
                    public void run() {
                        try {
                            r.run();
                        } finally {
                            scheduleNext();
                        }
                    }
                });
                if (active == null) {
                    scheduleNext();
                }
            }
        }

        public synchronized void execute(final Runnable r) {
            if (ensure(r)) {
                tasks.offer(new Runnable() {
                    public void run() {
                        try {
                            r.run();
                        } finally {
                            scheduleNext();
                        }
                    }
                });
                if (active == null) {
                    scheduleNext();
                }
            }
        }


        protected synchronized void scheduleNext() {
            if ((active = tasks.poll()) != null) {
                executor.execute((Runnable) active);
            }
        }
    }

}
