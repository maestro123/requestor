package maestro.requestor;

import org.apache.http.HttpHost;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.util.Locale;
import java.util.Properties;

/**
 * Created by Artyom on 10/8/2014.
 */
public class ClientGetter {

    public HttpClient getClient() {

        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

        HttpProtocolParams.setUseExpectContinue(params, true);

        Properties prop = java.lang.System.getProperties();
        String template_user_agent = "%s/%s (%s; U; Android %s; %s; %s Build/%s; FW %s)";

        String sUserAgent = String.format(template_user_agent,
                prop.getProperty("java.vm.name"),
                prop.getProperty("java.vm.version"),
                prop.getProperty("os.name"), android.os.Build.VERSION.RELEASE,
                Locale.getDefault().getLanguage(), android.os.Build.MODEL,
                android.os.Build.ID, android.os.Build.DISPLAY);

        HttpProtocolParams.setUserAgent(params, sUserAgent);

        ConnManagerParams.setMaxTotalConnections(params, 100);
        ConnPerRouteBean connperroutebean = new ConnPerRouteBean(20);
        connperroutebean.setMaxForRoute(new HttpRoute(new HttpHost("localhost", 80)), 50);
        ConnManagerParams.setMaxConnectionsPerRoute(params, connperroutebean);
        ConnManagerParams.setTimeout(params, 10000L);

        HttpConnectionParams.setSoTimeout(params, 40000);
        HttpConnectionParams.setConnectionTimeout(params, 40000);
        HttpClientParams.setRedirecting(params, true);

        SchemeRegistry schReg = new SchemeRegistry();

        schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

        ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);

        return new DefaultHttpClient(conMgr, params);
    }

    public final HttpRequestBase getBaseRequest(Requestor.Request request, String url) {
        return getBaseRequest(request.Type, url, request.Format);

    }

    public final HttpRequestBase getBaseRequest(Requestor.Request.REQUEST_TYPE requestType, String url, String format) {
        switch (requestType) {
            case POST:
                return buildRequest(new HttpPost(url), format);
            case PUT:
                return buildRequest(new HttpPut(url), format);
            case GET:
            default:
                return buildRequest(new HttpGet(url), format);
        }
    }

    public HttpRequestBase buildRequest(HttpRequestBase baseRequest, String format) {
        if (format == null) format = "application/xml";
        baseRequest.setHeader("Accept", format);
        baseRequest.setHeader("Content-type", new StringBuilder(format).append("; charset=utf-8").toString());
        baseRequest.setHeader("Accept-Encoding", "gzip");
        return baseRequest;
    }

}
